import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  ListView,
  StyleSheet,
  Alert,
} from 'react-native';
import TaskRow from './TaskRow/Component';


export default class TaskList extends Component {
	constructor(props, context) {
		super(props, context);
		const ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2,
		});

		this.state = {
			dataSource: ds.cloneWithRows(this.props.todos),
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({dataSource: this.state.dataSource.cloneWithRows(nextProps.todos)});
	}

	render() {
		return (
			<View style={styles.listContainer }>
				<Text style={styles.title}> {this.props.title} </Text>
				<ListView 
					dataSource = {this.state.dataSource}
					renderRow = {this.renderRow.bind(this)}/>
			</View>
		);
	}

	onTaskCompleted(todo) {
    	this.props.onTaskCompleted(todo);
  	}

	renderRow(todo, sectionID, rowID, highlightRow) {
		return (
			<TaskRow todo={todo} rowID={parseInt(rowID)} onTaskCompleted={this.onTaskCompleted.bind(this)} />
		);
	}
}

TaskList.propTypes = {
	todos: PropTypes.array.isRequired
};

const styles = StyleSheet.create({
  listContainer: {
    paddingTop: 20,
  },
  title: {
  	color: 'white',
  	marginBottom: 10,
  },
});