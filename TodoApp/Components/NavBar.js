import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class NavBar extends Component {
	constructor(props, context) {
		super(props, context);
	}
	render() {
		return (
			<View style={styles.rowContainer }>
				<Icon name="menu" size={30} color="white" style={styles.iconSection} />
				<Text style={styles.mainText}> {this.props.title} </Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  mainText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
    margin: 'auto',
  },
  iconSection: {
  	paddingLeft: 10,
  },
  rowContainer: {
  	paddingTop: 20,
  	flexDirection: 'row',
  	alignItems: 'center',
  },
});