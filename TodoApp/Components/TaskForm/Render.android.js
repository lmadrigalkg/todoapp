import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
} from 'react-native';

export default function render(styles) {
	return (
		<View style={styles.container}>
			<Text style={styles.mainText}> Add a task </Text>
      <TextInput 
        style={styles.textInput}
        value={this.state.text} 
        onChangeText={(text) => this.setState({text})}/>
      <TouchableHighlight style={styles.button} onPress={this.onAddPressed.bind(this)} underlayColor={'transparent'}>
        <Text style={styles.buttonText}> Add </Text>
      </TouchableHighlight>
      <TouchableHighlight style={[styles.button, styles.buttonCancel]} onPress={this.onCancelPressed.bind(this)} underlayColor={'transparent'}>
        <Text style={styles.buttonText}> Cancel </Text>
      </TouchableHighlight>
		</View>
	);
}