import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  StyleSheet,
  Button,
} from 'react-native';

export default function render(styles) {
	return (
		<View style={styles.container}>
			<Text style={styles.mainText}> Add a task </Text>
      <TextInput 
        style={[styles.textInput, iosStyles.input]}
        value={this.state.text} 
        onChangeText={(text) => this.setState({text})}/>
      <View style={iosStyles.buttonsRow}>
        <Button title="Add" onPress={this.onAddPressed.bind(this)} />
        <Button title="Cancel" color="#ef7a91" onPress={this.onCancelPressed.bind(this)} />
      </View>
		</View>
	);
}

const iosStyles = StyleSheet.create({
  input: {
    height: 50,
  },
  buttonsRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonContainer: {
    padding: 10,
  }
});
