import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  Alert,
} from 'react-native';
import Swipeout from 'react-native-swipeout';


export default function render(baseStyles) {
	const swipeButtons = [
		{
			text: 'Done',
			backgroundColor: '#05A5D1',
			underlayColor: '#273539',
			onPress: this.onDonePressed.bind(this)
		}
	];

	return (
		<View style={iosStyles.container}>
			<Swipeout
				backgroundColor='#fff'
				right={swipeButtons}>
				<View style={baseStyles.rowContainer}>
		        	<Text style={baseStyles.rowText}> { this.props.rowID + 1}. {this.props.todo.task} </Text>
	      		</View>
			</Swipeout>
		</View>
	);
}

const iosStyles = StyleSheet.create({
	container: {
		marginBottom: 5,
	},
});