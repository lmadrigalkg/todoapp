import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  Animated,
  Alert,
} from 'react-native';

export default function(baseStyles) {
	const doneAnimation = new Animated.ValueXY();
	const androidStyles = StyleSheet.create({
		row: {
			transform: doneAnimation.getTranslateTransform(),
		},
	});
	function animatedPressed() {
		Animated.spring(doneAnimation, {
			tension: 2,
			friction: 3,
			toValue: {
				x: -1500,
				y: 0,
			},
		}).start();

		setTimeout(() => {this.onDonePressed();}, 1000);
	}

	return (
		<Animated.View style={[baseStyles.rowContainer, androidStyles.row]}>
	        <Text style={baseStyles.rowText}> { this.props.rowID + 1}. {this.props.todo.task} </Text>
	        <TouchableHighlight style={baseStyles.doneLabel} onPress={animatedPressed.bind(this)} underlayColor={'transparent'}>
	          <Text style={baseStyles.doneText}> Done </Text>
	        </TouchableHighlight>
      	</Animated.View>	
	);
}