import * as types from './actionTypes';
import todoApi from './../todoMock';
import store from './../Store/todoStore';

export function getTodosSuccess(todos){
  return {type: types.GET_TODOS, todos};
}

export function addTodoSuccess(todos){
  return {type: types.ADD_TODO_SUCCESS, todos};
}
export function addTodoFailure(err){
  return {type: types.ADD_TODO_FAILURE, err};
}

export function deleteTodoFailure(err){
  return {type: types.DELETE_TODO_FAILURE, err};
}
export function deleteTodoSuccess(todos){
  return {type: types.DELETE_TODO_SUCCESS, todos};
}

export function getTodos(){
  return function(dispatch){
    return todoApi.getTodos().then(todos => {
      dispatch(getTodosSuccess(todos));
    }).catch(error => {
      throw(error);
    });
  };
}

export function addTodo(todo){
  return function(dispatch){
    return todoApi.addTodo(todo).then(todos => {
      dispatch(addTodoSuccess(todos));
    }).catch(error => {
      dispatch(addTodoFailure(error));
      throw(error);
    });
  };
}

export function deleteTodo(todoId){
  return function(dispatch){
    return todoApi.deleteTodo(todoId).then(todos => {
      dispatch(deleteTodoSuccess(todos));
    }).catch(error => {
    dispatch(deleteTodoFailure(error));
      throw(error);
    });
  };
}