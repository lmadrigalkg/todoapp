const todos = [
  {
    id: 0,
    task: 'First task'
  },{
    id: 1,
    task: 'Second task'
  },{
    id: 2,
    task: 'Third task'
  }
];

class TodoApi {
  static getTodos() {
    return new Promise((resolve,reject) => {
      resolve(Object.assign([], todos));
    });
  }

  static addTodo(todo) {
    var todo = Object.assign({}, todo); // to avoid manipulating object passed in.
    return new Promise ((resolve, reject) => {
      todos.push(todo);
      resolve(todos);
    });
  }

  static deleteTodo(finishedTodoId){
    return new Promise ((resolve, reject) => {
      todos = Object.assign([], todos.filter((todo) => {
        return todo.id != finishedTodoId;
      }));
      resolve(todos);
    });
  }
}

export default TodoApi;


